let switchN = 0;
let switchW = true;
let intervalId;
let imagesWrapper = document.querySelectorAll('.image-to-show');

showImage(0);
function showImage(n){
    imagesWrapper.forEach(element => {
        element.style.display = 'none';
    }); 
    imagesWrapper[n].style.display = 'flex';
    switchN++;
    if (switchN === imagesWrapper.length) {
        switchN = 0;
    }   
}

intervalId = setInterval(() => {
    showImage(switchN)
  }, 3000);
function switchWrapper(){
    if(switchW){
        console.log('1');
        clearInterval(intervalId);
        switchW = !switchW;
        btnStop.innerHTML = 'start';
    }
    else{
        intervalId = setInterval(() => {
            showImage(switchN)}, 3000);
            switchW = !switchW;
            btnStop.innerHTML = 'stop';
        }
          console.log('2');

}
let btnStop = document.createElement('button');
btnStop.innerHTML = 'stop';
document.querySelector('.images-wrapper').appendChild(btnStop);

btnStop.addEventListener('click', switchWrapper);
        
//hw-14

if (localStorage.getItem('btnColor') == null)
    localStorage.setItem('btnColor', 'blue');
if (localStorage.getItem('bodyColor') == null)
    localStorage.setItem('bodyColor', 'green');
if (localStorage.getItem('themeBtnColor') == null)
    localStorage.setItem('themeBtnColor', 'red');
if (localStorage.getItem('themeBodyColor') == null)
    localStorage.setItem('themeBodyColor', 'gray');
if (localStorage.getItem('__theme') == null)
    localStorage.setItem('__theme', 0);

document.querySelector('.images-wrapper button').style.backgroundColor = localStorage.getItem('btnColor');
document.body.style.backgroundColor = localStorage.getItem('bodyColor');

function changeTheme(){
    let btnTemp, bodyTemp;
    if(localStorage.getItem('__theme')){
        btnTemp = localStorage.getItem('themeBtnColor');
        bodyTemp = localStorage.getItem('themeBodyColor');
        localStorage.setItem('themeBtnColor', localStorage.getItem('btnColor'));
        localStorage.setItem('themeBodyColor', localStorage.getItem('bodyColor'));
        localStorage.setItem('btnColor', btnTemp);
        localStorage.setItem('bodyColor', bodyTemp);
        localStorage.setItem('__theme', 1);

        document.querySelector('.images-wrapper button').style.backgroundColor = localStorage.getItem('btnColor');
        document.body.style.backgroundColor = localStorage.getItem('bodyColor');
    }
    else{
        btnTemp = localStorage.getItem('themeBtnColor');
        bodyTemp = localStorage.getItem('themeBodyColor');
        localStorage.setItem('themeBtnColor', localStorage.getItem('btnColor'));
        localStorage.setItem('themeBodyColor', localStorage.getItem('bodyColor'));
        localStorage.setItem('btnColor', btnTemp);
        localStorage.setItem('bodyColor', bodyTemp);
        localStorage.setItem('__theme', 0);

        document.querySelector('.images-wrapper button').style.backgroundColor = localStorage.getItem('btnColor');
        document.body.style.backgroundColor = localStorage.getItem('bodyColor');
    }
}

let btnChangeTheme = document.createElement('button');
btnChangeTheme.innerHTML = 'Change Theme';
document.body.appendChild(btnChangeTheme);


btnChangeTheme.addEventListener('click', changeTheme);